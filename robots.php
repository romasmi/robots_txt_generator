<?php

header("Content-type: text/plain; charset=UTF-8");

$domain = $_SERVER['SERVER_NAME'];

$content = "User-agent: *
Disallow: *page=
Disallow: *product_id=
Disallow: *specs_values=
Disallow: *mfp=
Disallow: /credit?
Disallow: *sort=
Disallow: /all_goods_photos?
Host: https://{$domain}
Sitemap: https://{$domain}/sitemap_index.xml
";
echo $content;



